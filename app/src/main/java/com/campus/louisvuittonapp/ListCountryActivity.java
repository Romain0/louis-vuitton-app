package com.campus.louisvuittonapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class ListCountryActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_town);
    }

    // to return at first page of app
    public void homeFunction (View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
    }

    // to back at precedent page in app
    public void backFunction (View view)
    {
        super.onBackPressed();
    }
}