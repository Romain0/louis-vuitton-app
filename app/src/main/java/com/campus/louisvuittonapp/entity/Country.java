package com.campus.louisvuittonapp.entity;

import org.parceler.Parcel;

import java.io.Serializable;

@Parcel
public class Country implements Serializable
{
    public int id;
    public String name;
    public int imgPath;
    public int imgTitle;
    public int imgBackground;
    public String[] music_style;
    public String information;
    public double lat;
    public double lon;
    public String country_id;

    // Constructors
    public Country() {}

    public Country(int id, String name, int imgPath, int imgTitle, int imgBackground, String[] music_style, String information, double lat, double lon, String country_id)
    {
        this.id = id;
        this.name = name;
        this.imgPath = imgPath;
        this.imgBackground = imgBackground;
        this.imgTitle = imgTitle;
        this.music_style = music_style;
        this.information = information;
        this.lat = lat;
        this.lon = lon;
        this.country_id = country_id;
    }
}
