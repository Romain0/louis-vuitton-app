package com.campus.louisvuittonapp;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import com.campus.louisvuittonapp.adapter.CountryAdapter;
import com.campus.louisvuittonapp.entity.Country;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.parceler.Parcels;

import java.util.ArrayList;

public class MapActivity extends AppCompatActivity implements LocationListener
{
    private static Context context;

    public static Context getContext() {
        return context;
    }

    private MapView map; // Carte

    private ArrayList<Country> countryList = new ArrayList<>(); // Liste des Pays

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Configuration.getInstance().load(
                getApplicationContext(),
                PreferenceManager.getDefaultSharedPreferences( getApplicationContext() )
        );
        setContentView(R.layout.activity_map);

        // get countryList
        countryList = Parcels.unwrap(getIntent().getParcelableExtra(CountryAdapter.getTown_DETAILS_KEY()));


        map = findViewById( R.id.map );
        map.setTileSource( TileSourceFactory.MAPNIK );  // Rendue
        map.setMultiTouchControls( true );// Zoomable
        map.setMinZoomLevel(2d);

        IMapController mapController = map.getController(); // Controleur de la carte
        mapController.setZoom( 15.0 ); // zoom par default
        showMyLocation(mapController); // centre par default geolocalisation
        addMarkers();
    }

    public void addMarkers()
    {
        for (int i = 0; i < countryList.size(); i++)
        {
            Marker m = new Marker(map);
            m.setIcon(ContextCompat.getDrawable(getApplication(), R.drawable.ic_marker_foreground));
            m.setImage(ContextCompat.getDrawable(getApplication(), R.mipmap.ic_launcher_logo_app));
            m.setPosition(new GeoPoint(countryList.get(i).lat, countryList.get(i).lon));
            m.setTitle(countryList.get(i).name);
            m.setSubDescription(countryList.get(i).information);
            map.getOverlays().add(m);
        }
    }
    @Override
    public void onPause()
    {
        super.onPause();
        map.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        map.onResume();
    }

    // to return at first page of app
    public void homeFunction (View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
    }

    // to back at precedent page in app
    public void backFunction (View view)
    {
        super.onBackPressed();
    }


    // Find Location provider is openning.
    private String getEnabledLocationProvider() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // Criteria to find location provider.
        Criteria criteria = new Criteria();

        // Returns the name of the provider that best meets the given criteria.
        // ==> "gps", "network",...
        String bestProvider = locationManager.getBestProvider(criteria, true);

        assert bestProvider != null;
        boolean enabled = locationManager.isProviderEnabled(bestProvider);

        if (!enabled) {
            return null;
        }
        return bestProvider;
    }

    // Call this method only when you have the permissions to view a user's location.
    private void showMyLocation(IMapController mapController) {

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        String locationProvider = this.getEnabledLocationProvider();

        if (locationProvider == null) {
            Toast.makeText(getApplicationContext(),"Impossible d'utiliser le GPS de l'appareil",Toast.LENGTH_LONG).show();
            Intent intent = new Intent(this, MainActivity.class);
            this.startActivity(intent);
            //return;
        }

        // Millisecond
        final long MIN_TIME_BW_UPDATES = 1000;
        // Met
        final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;

        Location myLocation;
        try {
            // This code need permissions (Asked above ***)
            locationManager.requestLocationUpdates(
                    locationProvider,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, (LocationListener) this);
            // Getting Location.
            // Lấy ra vị trí.
            myLocation = locationManager
                    .getLastKnownLocation(locationProvider);
        }
        // With Android API >= 23, need to catch SecurityException.
        catch (SecurityException e) {
            Log.e("MYTAG", "Show My Location Error:" + e.getMessage());
            e.printStackTrace();
            return;
        }

        if (myLocation != null) {

            GeoPoint latLng = new GeoPoint(myLocation.getLatitude(), myLocation.getLongitude());
            mapController.setCenter(latLng);

        } else {
            Log.i("MYTAG", "Location not found");
        }
    }

    @Override
    public void onLocationChanged(Location location) { }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) { }

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) { }

}