package com.campus.louisvuittonapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.campus.louisvuittonapp.adapter.CountryAdapter;
import com.campus.louisvuittonapp.entity.Country;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

public class DetailCountryActivity extends AppCompatActivity
{
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_town);

        Context context = getApplicationContext();

        View layoutDetail = findViewById(R.id.layoutDetail);
        TextView txtIntutileDetail = findViewById(R.id.intutileDetail);
        TextView txtTownDetails = findViewById(R.id.txtTownDetails);
        ImageView imgTownDetails = findViewById(R.id.imgTownDetails);

        Country countrySent = Parcels.unwrap(getIntent().getParcelableExtra(CountryAdapter.getTown_DETAILS_KEY()));
        layoutDetail.setBackground(getResources().getDrawable(countrySent.imgBackground));

        int imagePath = countrySent.imgTitle;
        // img with picasso
        Picasso.with(context)
                .load(imagePath)
                .into(imgTownDetails);

        String name = countrySent.name;
        txtIntutileDetail.setText(name);
        String info = countrySent.information;
        txtTownDetails.setText(info);
        // txtTownDetails.setTag(townSent);
    }

    // to return at first page of app
    public void homeFunction (View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        this.startActivity(intent);
    }

    // to back at precedent page in app
    public void backFunction (View view)
    {
        super.onBackPressed();
    }
}