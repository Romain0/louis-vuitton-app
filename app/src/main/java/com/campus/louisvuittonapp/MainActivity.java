package com.campus.louisvuittonapp;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.campus.louisvuittonapp.adapter.CountryAdapter;
import com.campus.louisvuittonapp.entity.Country;

import org.parceler.Parcels;

import java.security.Permission;
import java.util.ArrayList;
import java.util.List;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{

    private List<Country> countryList = new ArrayList<Country>();
    public static final int REQUEST_ID_ACCESS_COURSE_FINE_LOCATION = 100;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Configuration.getInstance().load(
                getApplicationContext(),
                PreferenceManager.getDefaultSharedPreferences( getApplicationContext() )
        );
        setContentView(R.layout.activity_main);

        // disable back arrow because it's the first page
        ImageView backArrow = findViewById(R.id.backArrow);
        backArrow.setVisibility(View.GONE);

        // init Town object
        countryList.add(new Country(1, "FRANCE", R.drawable.maquette_france, R.drawable.fond_france, R.drawable.illustration_france, new String[]{"Classique"}, "Paris - Test de description", 48.85341, 2.3488, "FR"));
        countryList.add(new Country(2, "ANGLETERRE", R.drawable.maquette_angleterre, R.drawable.fond_angleterre,R.drawable.illustration_angleterre, new String[]{"Rock"}, "Londres - Test de description", 51.509865, -0.118092, "EN"));
        countryList.add(new Country(3, "JAPON", R.drawable.maquette_japon, R.drawable.fond_angleterre,R.drawable.illustration_angleterre, new String[]{"nc"}, "Pékin - Test de description", 39.5426, 116.2350, "JP"));
        countryList.add(new Country(4, "ETAT-UNIS", R.drawable.maquette_etats_unis, R.drawable.fond_angleterre,R.drawable.illustration_angleterre, new String[]{"nc"}, "New York - Test de description", 40.730610, -73.935242, "US"));
        countryList.add(new Country(5, "MAROC", R.drawable.maquette_maroc_, R.drawable.fond_angleterre,R.drawable.illustration_angleterre, new String[]{"nc"}, "Rabat - Test de description", 31.7917, -7.0926, "nc"));
        countryList.add(new Country(6, "MEXIQUE", R.drawable.maquette_mexique_, R.drawable.fond_angleterre,R.drawable.illustration_angleterre, new String[]{"nc"}, "Mexico - Test de description", 23.6345, -102.5528, "MX"));
        countryList.add(new Country(7, "ITALIE", R.drawable.maquette_italie, R.drawable.fond_angleterre,R.drawable.illustration_angleterre, new String[]{"classic"}, "Rome - Test de description", 41.890251, 12.492373, "IT"));
        countryList.add(new Country(8, "COREE DU SUD", R.drawable.maquette__cor_e_du_sud, R.drawable.fond_angleterre,R.drawable.illustration_angleterre, new String[]{"nc"}, "Séoul - Test de description", 36.332828, 127.397461, "nc"));

        // shadow start
        ImageView img = findViewById(R.id.imageView2);
        img.setTranslationZ(15);
        // shadow end

        CountryAdapter countryAdapter = new CountryAdapter(countryList);

        // Recycler view
        RecyclerView recyclerView = findViewById(R.id.listTown);

        // better perform
        recyclerView.setHasFixedSize(true);

        // layout manager, décrivant comment les items sont disposés :
        //LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(countryAdapter);
    }

    public void showFirst(View view)
    {
        Intent intent = new Intent(this, DetailCountryActivity.class);
        intent.putExtra(CountryAdapter.getTown_DETAILS_KEY(), Parcels.wrap(countryList.get(0)));
        this.startActivity(intent);
    }

    public void showMap(View view) {
        if (Build.VERSION.SDK_INT >= 23) {
            int accessCoarsePermission
                    = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int accessFinePermission
                    = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);


            if (accessCoarsePermission != PackageManager.PERMISSION_GRANTED
                    || accessFinePermission != PackageManager.PERMISSION_GRANTED) {
                // The Permissions to ask user.
                String[] permissions = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION};
                // Show a dialog asking the user to allow the above permissions.
                ActivityCompat.requestPermissions(this, permissions,
                        REQUEST_ID_ACCESS_COURSE_FINE_LOCATION);

                return;
            }else{
                Intent intent = new Intent(this, MapActivity.class);
                intent.putExtra(CountryAdapter.getTown_DETAILS_KEY(), Parcels.wrap(countryList));
                this.startActivity(intent);
            }
        }

    }


    // to return at first page of app
    public void homeFunction (View view)
    {
        // void
    }

    // to back at precedent page in app
    public void backFunction (View view)
    {
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_ID_ACCESS_COURSE_FINE_LOCATION: {

                // Note: If request is cancelled, the result arrays are empty.
                // Permissions granted (read/write).
                if (grantResults.length > 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(this, MapActivity.class);
                    intent.putExtra(CountryAdapter.getTown_DETAILS_KEY(), Parcels.wrap(countryList));
                    this.startActivity(intent);
                }
                // Cancelled or denied.
                else {
                    Toast.makeText(this, "Permissions Refusées!", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }
}