package com.campus.louisvuittonapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.campus.louisvuittonapp.DetailCountryActivity;
import com.campus.louisvuittonapp.R;
import com.campus.louisvuittonapp.entity.Country;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.TownViewHolder>
{
    public static final String TOWN_DETAILS_KEY = "123";
    private List<Country> listCountries;
    // private FrameLayout frameLayoutConteneurDetail = null;

    // Constructor
    public CountryAdapter(List<Country> listCountries)
    {
        this.listCountries = listCountries;
    }

    @Override
    public TownViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View viewTown = LayoutInflater.from(parent.getContext()).inflate(R.layout.town, parent, false);
        return new TownViewHolder(viewTown);
    }

    @Override
    public void onBindViewHolder(TownViewHolder holder, int position)
    {
        String name = listCountries.get(position).name;
        int imagePath = listCountries.get(position).imgPath;
        holder.townTxTView.setText(name);

        // img with picasso
        Picasso.with(holder.townImageView.getContext())
                .load(imagePath)
                .into(holder.townImageView);
        /*.fit()
        .centerCrop()*/
    }

    @Override
    public int getItemCount()  { return listCountries.size(); }

    public class TownViewHolder extends RecyclerView.ViewHolder
    {
        public TextView townTxTView;
        public ImageView townImageView;

        public TownViewHolder(@NonNull View itemView)
        {
            super(itemView);
            townTxTView = itemView.findViewById(R.id.townTxTView);
            townImageView = itemView.findViewById(R.id.townImageView);

            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Context context = view.getContext();
                    Intent intent = new Intent(context, DetailCountryActivity.class);
                    // send object to detailActivity with Parceler library
                    intent.putExtra(TOWN_DETAILS_KEY, Parcels.wrap(listCountries.get(getAdapterPosition())));
                    context.startActivity(intent);
                }
            });
        }
    }

    public static String getTown_DETAILS_KEY() { return TOWN_DETAILS_KEY; }
}
